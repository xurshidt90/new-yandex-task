package uz.xsoft.newtaskyandex.fragments;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

import uz.xsoft.newtaskyandex.MyViewModel;
import uz.xsoft.newtaskyandex.R;
import uz.xsoft.newtaskyandex.adapters.AdapterSecond;
import uz.xsoft.newtaskyandex.models.Model;

public class FragmentSecond extends Fragment implements AdapterSecond.OnClickItemListiner {

    private RecyclerView list;
    private ImageView img;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        ViewModelProviders.of(getActivity()).get(MyViewModel.class).setPositionLiveData(0);

        View view = inflater.inflate(R.layout.layout_fragment_second,container,false);
        list = view.findViewById(R.id.list);
        img = view.findViewById(R.id.img);
        final AdapterSecond adapter = new AdapterSecond(new ArrayList<Model>());
        list.setAdapter(adapter);
        list.setLayoutManager(new GridLayoutManager(getContext(),3));
        ViewModelProviders.of(getActivity()).get(MyViewModel.class).getModelLiveData().observe(getActivity(), new Observer<List<Model>>() {
            @Override
            public void onChanged(@Nullable List<Model> models) {
                if (models != null|| models.size()!=0){
                    adapter.change(models);
                    img.setVisibility(View.INVISIBLE);
                }else {
                    img.setVisibility(View.VISIBLE);
                }
            }
        });
        adapter.setOnClickItemListiner(this);

        return view;
    }

    @Override
    public void onItemClick(Model model, int position) {
        ViewModelProviders.of(getActivity()).get(MyViewModel.class).setPositionLiveData(position);
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ShowImage showImage = new ShowImage();
        showImage.show(ft,"show");

        getFragmentManager().registerFragmentLifecycleCallbacks(new FragmentManager.FragmentLifecycleCallbacks() {
            @Override
            public void onFragmentViewDestroyed(FragmentManager fm, Fragment f) {
                super.onFragmentViewDestroyed(fm, f);
                //do sth
                scrollToPosition();
                getFragmentManager().unregisterFragmentLifecycleCallbacks(this);
            }
        }, false);

        Log.d("salom", "onItemClick: show image");
    }

    private void scrollToPosition() {
        list.scrollToPosition(ViewModelProviders.of(getActivity()).get(MyViewModel.class).getPositionLiveData().getValue());
    }
}
