package uz.xsoft.newtaskyandex.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import uz.xsoft.newtaskyandex.R;


public class ImageFragment extends Fragment {


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.layout_image, container, false);

        Log.d("salom", "onCreateView: " + getArguments().getString("url"));

        ImageView imageView = view.findViewById(R.id.img);
        Picasso.get().load(getArguments().getString("url")).into(imageView);


        return view;
    }
}
