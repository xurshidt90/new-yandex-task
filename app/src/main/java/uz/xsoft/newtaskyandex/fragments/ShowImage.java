package uz.xsoft.newtaskyandex.fragments;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import uz.xsoft.newtaskyandex.MyViewModel;
import uz.xsoft.newtaskyandex.R;


public class ShowImage extends DialogFragment {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_FRAME, android.R.style.Theme_Holo_Light);
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.layout_show_image, container, false);

        Toolbar toolbar = view.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setNavigationOnClickListener(v -> dismiss());

        ViewPager viewPager = view.findViewById(R.id.viewpager);

        ShowImageAdapter adapter = new ShowImageAdapter(getChildFragmentManager(), ViewModelProviders.of(getActivity()).get(MyViewModel.class).getModelLiveData().getValue());

        viewPager.setAdapter(adapter);
        Log.d("salom", "onCreateView: " + ViewModelProviders.of(getActivity()).get(MyViewModel.class).getModelLiveData().getValue().size());
        Log.d("salom", "onCreateView: " + ViewModelProviders.of(getActivity()).get(MyViewModel.class).getPositionLiveData().getValue());

        viewPager.setCurrentItem(ViewModelProviders.of(getActivity()).get(MyViewModel.class).getPositionLiveData().getValue());


        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                ViewModelProviders.of(getActivity()).get(MyViewModel.class).setPositionLiveData(position);
                Log.d("salom", "onPageSelected: "+position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        return view;
    }
}
