package uz.xsoft.newtaskyandex.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

import uz.xsoft.newtaskyandex.models.Model;


public class ShowImageAdapter extends FragmentPagerAdapter {


    private List<Model> data;
    public ShowImageAdapter(FragmentManager fm, List<Model> data) {
        super(fm);
        this.data = data;
    }

    @Override
    public Fragment getItem(int position) {
        ImageFragment fragment = new ImageFragment();
        Bundle bundle = new Bundle();
        bundle.putString("url",data.get(position).getFilename());
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getCount() {
        return data.size();
    }
}
