package uz.xsoft.newtaskyandex.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import uz.xsoft.newtaskyandex.R;
import uz.xsoft.newtaskyandex.models.Model;


public class AdapterSecond extends RecyclerView.Adapter<AdapterSecond.MyViewHolder> {


    private List<Model> data;
    private OnClickItemListiner onClickItemListiner;

    public interface OnClickItemListiner {
        void onItemClick(Model model, int position);
    }

    public void setOnClickItemListiner(OnClickItemListiner onClickItemListiner) {
        this.onClickItemListiner = onClickItemListiner;

    }

    public AdapterSecond(List<Model> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_second,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.bind(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void change(List<Model> models) {
        data = models;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        TextView tv;
        ImageView img;
        public MyViewHolder(View itemView) {
            super(itemView);
            tv = itemView.findViewById(R.id.tv);
            img = itemView.findViewById(R.id.img);
        }

        public void bind(Model model){
            tv.setText(model.getAuthor());
            Picasso.get().load(model.getFilename()).into(img);
            itemView.setOnClickListener(v -> {
                if (onClickItemListiner != null) {
                    onClickItemListiner.onItemClick(model,getAdapterPosition());
                }
            });
        }
    }
}
