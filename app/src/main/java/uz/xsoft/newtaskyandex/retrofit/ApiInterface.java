package uz.xsoft.newtaskyandex.retrofit;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import uz.xsoft.newtaskyandex.models.Model;

public interface ApiInterface {
    @GET("/list")
    Call<List<Model>> getAllImage();
}
