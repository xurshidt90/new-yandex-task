package uz.xsoft.newtaskyandex;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.util.Log;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uz.xsoft.newtaskyandex.models.Model;
import uz.xsoft.newtaskyandex.retrofit.ApiClient;
import uz.xsoft.newtaskyandex.retrofit.ApiInterface;


public class MyViewModel extends ViewModel {

    private MutableLiveData<Integer> positionLiveData = new MutableLiveData<>();
    private MutableLiveData<List<Model>> modelLiveData = new MutableLiveData<>();


    public MutableLiveData<Integer> getPositionLiveData() {
        if (positionLiveData.getValue()==null)
            positionLiveData.setValue(0);
        return positionLiveData;
    }

    public void setPositionLiveData(int position) {
        this.positionLiveData.setValue(position);
    }

    public MutableLiveData<List<Model>> getModelLiveData() {
        if (modelLiveData.getValue()==null){
            loadFromServer();
        }
        return modelLiveData;
    }


    public void setModelLiveData(List<Model> modelLiveData) {
        this.modelLiveData.postValue(modelLiveData);
    }


    private void loadFromServer() {
        ApiInterface api = ApiClient.getClient().create(ApiInterface.class);
        Call<List<Model>> call = api.getAllImage();
        call.enqueue(new Callback<List<Model>>() {
            @Override
            public void onResponse(Call<List<Model>> call, Response<List<Model>> response) {
                if (response.isSuccessful()){
                    modelLiveData.postValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<Model>> call, Throwable t) {
                Log.d("salom", "onFailure: "+t.toString());
            }
        });
    }

}
