package uz.xsoft.newtaskyandex;


import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import uz.xsoft.newtaskyandex.fragments.FragmentOne;
import uz.xsoft.newtaskyandex.fragments.FragmentSecond;
import uz.xsoft.newtaskyandex.fragments.FragmentThird;


public class ViewPagerAdapter extends FragmentPagerAdapter {

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return new FragmentOne();
        }
        if (position == 1) {
            return new FragmentSecond();

        }
        else {
            return new FragmentThird();
        }
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        if (position == 0) {
            return "Лента";
        }
        if (position == 1) {
            return "Офлайн";
        } else {
            return "Все фото";
        }
    }
}

